import 'dart:convert';

import 'package:parkingclientapp/config.dart';
import 'package:parkingclientapp/louisk/base_provider.dart';
import 'package:parkingclientapp/models/client_models.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends BaseProvider {
  Future<ClientModel> login(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};

    final response = await super.post('login', body);

    return ClientModel.fromJson(response);
  }

  Future<ClientModel> register(String name, String email, String password,
      String passwordConfirmation) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Map<String, dynamic> body = {
      'name': name,
      'email': email,
      'password': password,
      'password_confirmation': passwordConfirmation
    };

    final response = await super.post('register', body);

    prefs.setString('profile', json.encode(response['data']));

    return ClientModel.fromJson(response['data']);
  }

  Future<ClientModel> getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String profile = prefs.getString('profile');

    if (profile != null && profile.length > 0) {
      return ClientModel.fromJson(json.decode(profile));
    }

    final response = await super.get('me');

    prefs.setString('profile', json.encode(response['data']));

    return ClientModel.fromJson(response['data']);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove('profile');
    prefs.remove(JWT_TOKEN);
  }

  Future<ClientModel> update(Map<String, String> data) async {
    final response = await super.put('me', data);

    return ClientModel.fromJson(response['data']);
  }
}
