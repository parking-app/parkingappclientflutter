import 'package:parkingclientapp/louisk/base_provider.dart';
import 'package:parkingclientapp/models/parking_models.dart';

class ParkingProvider extends BaseProvider {
  String _baseUrl = 'parking';

  Future<ParkingList> fetchParkingList(
      {Map<String, dynamic> query = const {}}) async {
    final response = await super.get(_baseUrl, query: query);

    return ParkingList.fromJson(response);
  }
}
