import 'dart:io';

import 'package:flutter/material.dart';
import 'package:parkingclientapp/pages/login_page/login_page.dart';
import 'package:parkingclientapp/pages/parkings_list_page/parkings_list.dart';
import 'package:parkingclientapp/pages/register_page/register_page.dart';
import 'package:parkingclientapp/providers/auth_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {


  @override
  MyAppState createState() {


    return new MyAppState();
  }
}

class MyAppState extends State<MyApp> {

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();


  @override
  initState(){
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true),
    );

    _firebaseMessaging.getToken().then((token) async {
      print(token);
      AuthProvider authProvider = new AuthProvider();

      var jwtToken = await authProvider.getToken();

      if(jwtToken != null){
        authProvider.update({
          'device_token': token,
          'device_model': Platform.isAndroid ? 'android' : 'ios'
        });
      }
    });
  }

  Widget getRootWidget() {
    AuthProvider authProvider = new AuthProvider();

    return new FutureBuilder(
      future: authProvider.getProfile(),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return ParkingListPage();
        }
        if (snapshot.hasError) {
          return LoginPage();
        }

        return new Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.amber,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Parking App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        '/main': (BuildContext context) => ParkingListPage(),
        '/login': (BuildContext context) => LoginPage(),
        '/register': (BuildContext context) => RegisterPage(),
      },
      debugShowCheckedModeBanner: false,
      home: getRootWidget(),
    );
  }
}
