import 'package:parkingclientapp/models/response_models.dart';

class ParkingModel {
  int _id;
  String _name;
  String _fullAddress;
  double _distance;
  double _longitude;
  double _latitude;
  double _averageRatings;
  int _totalVacancies;
  List<String> _photos;

  ParkingModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _fullAddress = json['full_address'];
    _totalVacancies = int.parse(json['total_vacancies'].toString());
    _distance = double.parse(json['distance'].toString()) ?? 0;
    _averageRatings = double.parse(json['average_ratings'].toString()) ?? 0;
    _longitude = json['longitude'] ?? 0;
    _latitude = json['latitude'] ?? 0;
    _photos = json['photos'] ?? [];
  }

  int get id => _id;

  int get totalVacancies => _totalVacancies;

  String get name => _name;

  String get fullAddress => _fullAddress;

  double get distance => _distance.roundToDouble();

  double get longitude => _longitude;

  double get latitude => _latitude;

  double get averageRatings => _averageRatings;

  List<String> get photos => _photos;
}

class ParkingList {
  ResponseMeta _meta;
  List<ParkingModel> _items;
  bool isLoading = false;

  ParkingList.isLoading() {
    _items = [];
    _meta = ResponseMeta();
    isLoading = true;
  }

  ParkingList.fromJson(Map<String, dynamic> json) {
    List<ParkingModel> items = [];

    for (int i = 0; i < json['data'].length; i++) {
      items.add(ParkingModel.fromJson(json['data'][i]));
    }

    _items = items;

    isLoading = false;

    _meta = ResponseMeta.fromJson(json);
  }

  static getBaseMeta(total) {
    Map<String, dynamic> meta = Map();

    meta['total'] = total;
    meta['current_page'] = 1;
    meta['last_page'] = 1;
    meta['to'] = 1;
    meta['from'] = 1;
  }

  ResponseMeta get meta => _meta;

  List<ParkingModel> get items => _items;
}
