class ClientModel {
  int _id;
  String _name;
  String _email;

  ClientModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
  }

  int get id => _id;

  String get name => _name;

  String get nameUpper {
    String firstLetter = _name.substring(0,1);

    return _name.replaceFirst(firstLetter, firstLetter.toUpperCase());
  }

  String get email => _email;
}
