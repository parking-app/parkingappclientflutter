class ResponseMeta {
  int _page;
  int _totalItems;
  int _lastPage;
  int _nextPage;
  int _prevPage;

  ResponseMeta() {
    _page = 1;
    _totalItems = 0;
    _lastPage = 1;
    _nextPage = 1;
    _prevPage = 1;
  }

  ResponseMeta.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> meta = json.containsKey('meta')
        ? json['meta']
        : ResponseMeta.getBaseMeta(json['data'].length);

    _lastPage = meta['last_page'];
    _totalItems = meta['total'];
    _page = meta['current_page'];
    _nextPage = meta['to'];
    _prevPage = meta['from'];
  }

  static Map<String, dynamic> getBaseMeta(total) {
    Map<String, dynamic> meta = Map();

    meta['total'] = total;
    meta['current_page'] = 1;
    meta['last_page'] = 1;
    meta['to'] = 1;
    meta['from'] = 1;

    return meta;
  }

  int get page => _page;

  int get totalItems => _totalItems;

  int get lastPage => _lastPage;

  int get nextPage => _nextPage;

  int get prevPage => _prevPage;
}
