import 'package:flutter/material.dart';
import 'package:parkingclientapp/louisk/base_provider.dart';
import 'package:parkingclientapp/louisk/loading_button.dart';
import 'package:parkingclientapp/louisk/validators.dart';
import 'package:parkingclientapp/providers/auth_provider.dart';

class LoginPage extends StatefulWidget {
  final AuthProvider authProvider = new AuthProvider();

  static const INPUT_EMAIL = 'email';
  static const INPUT_PASSWORD = 'password';

  @override
  _LoginPageState createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  bool _loading = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final Map<String, Map<String, dynamic>> controllers = {
    LoginPage.INPUT_EMAIL: {
      'errorText': null,
      'controller': new TextEditingController()
    },
    LoginPage.INPUT_PASSWORD: {
      'errorText': null,
      'controller': new TextEditingController()
    },
  };

  _validate() async {
    _clearErrorText();

    if (_formKey.currentState.validate()) {
      _sendForm();
    }
  }

  _sendForm() async {
    try {
      setState(() {
        _loading = true;
      });

      await widget.authProvider.login(
        controllers[LoginPage.INPUT_EMAIL]['controller'].text,
        controllers[LoginPage.INPUT_PASSWORD]['controller'].text,
      );

      Navigator.of(context).pushReplacementNamed('/main');
    } on ValidatorException catch (validator) {
      setState(() {
        controllers.forEach((key, input) {
          controllers[key]['errorText'] = validator.getMessage(key);
        });
      });
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }

  _clearErrorText() {
    setState(() {
      controllers.forEach((key, input) {
        controllers[key]['errorText'] = null;
      });
    });
  }

  _pushToRegister() {
    Navigator.of(context).pushReplacementNamed('/register');
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            controller: controllers[LoginPage.INPUT_EMAIL]['controller'],
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              labelText: 'Email',
              errorText: controllers[LoginPage.INPUT_EMAIL]['errorText'],
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
                ValidatorRules.Email
              ];

              Validator validator = Validator(rules, LoginPage.INPUT_EMAIL);

              return validator.validate(value);
            },
          ),
          TextFormField(
            controller: controllers[LoginPage.INPUT_PASSWORD]['controller'],
            decoration: InputDecoration(
              labelText: 'Senha',
              errorText: controllers[LoginPage.INPUT_PASSWORD]['errorText'],
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
              ];

              Validator validator = Validator(rules, 'password');

              return validator.validate(value);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Card(
            margin: EdgeInsets.symmetric(horizontal: 25, vertical: 100),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: _form(),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: LoadingButton(
                    RaisedButton(
                      onPressed: () => _validate(),
                      child: Text('Entrar'),
                    ),
                    _loading,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    onPressed: () => _pushToRegister(),
                    child: Text('Cadastrar'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
