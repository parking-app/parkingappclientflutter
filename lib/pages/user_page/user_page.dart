import 'package:flutter/material.dart';
import 'package:parkingclientapp/bloc/auth_bloc.dart';
import 'package:parkingclientapp/models/client_models.dart';
import 'package:parkingclientapp/providers/auth_provider.dart';

class UserPage extends StatefulWidget {
  final AuthProvider authProvider = new AuthProvider();

  @override
  UserPageState createState() {
    return new UserPageState();
  }
}

class UserPageState extends State<UserPage> {
  close() {
    Navigator.of(context).pop();
  }

  logout() {
    widget.authProvider.logout();
    Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
  }

  goToEditUser() {}

  showDialogEditPassword() {}

  @override
  Widget build(BuildContext context) {
    authBloc.fetchProfile();

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: StreamBuilder(
          stream: authBloc.profile,
          builder: (_, AsyncSnapshot<ClientModel> snapshot) {
            if (snapshot.hasData) {
              return Text(
                snapshot.data.nameUpper,
                style: TextStyle(color: Colors.black),
              );
            }

            return Center();
          },
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => close(),
          icon: Icon(Icons.close),
          color: Colors.black,
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              textTheme: ButtonTextTheme.normal,
              onPressed: () => goToEditUser(),
              child: Text('EDITAR PERFIL'),
            ),
            FlatButton(
              textTheme: ButtonTextTheme.normal,
              onPressed: () => showDialogEditPassword(),
              child: Text('EDITAR SENHA'),
            ),
            FlatButton(
              textTheme: ButtonTextTheme.normal,
              onPressed: () => logout(),
              child: Text('SAIR'),
            )
          ],
        ),
      ),
    );
  }
}
