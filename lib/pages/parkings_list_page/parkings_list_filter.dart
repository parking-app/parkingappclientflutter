import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ParkingListFilterPage extends StatefulWidget {
  final Function updateMeta;
  final int initialRadius;

  ParkingListFilterPage(this.updateMeta, {this.initialRadius = 3});

  @override
  _ParkingListFilterPageState createState() =>
      _ParkingListFilterPageState(this.initialRadius);
}

class _ParkingListFilterPageState extends State<ParkingListFilterPage> {
  int radius;

  _ParkingListFilterPageState(initialRadius){
    this.radius = initialRadius;

    SharedPreferences.getInstance().then((pref){
      int radius = pref.getInt('filter_radius');
      if(radius != null){
        setState(() => this.radius = radius);
      }

    });
  }

  save() async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    pref.setInt('filter_radius', radius);

    if (radius != widget.initialRadius) {

      widget.updateMeta(radius);
    }

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Filtros'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => save(),
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Slider(
            label: radius.toString(),
            value: radius.toDouble(),
            onChanged: (value) {
              setState(() => radius = value.toInt());
            },
            min: 1,
            max: 30,
            divisions: 30,
          ),
        ),
      ),
    );
  }
}
