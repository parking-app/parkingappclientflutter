import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:parkingclientapp/bloc/parking_bloc.dart';
import 'package:parkingclientapp/config.dart';
import 'package:parkingclientapp/models/parking_models.dart';

class MapListParking extends StatefulWidget {
  final double latitude;
  final double longitude;
  final Function sendSearch;

  MapListParking(this.sendSearch, this.latitude, this.longitude);

  @override
  MapListParkingState createState() {
    return new MapListParkingState();
  }
}

class MapListParkingState extends State<MapListParking> {
  List<Marker> markers = [];

  @override
  initState() {
    super.initState();
    if (widget.latitude != null && widget.longitude != null) {
      markers.add(buildMarker(widget.latitude, widget.longitude, isRoot: true));
    }
    widget.sendSearch(limit: 0);
  }

  Marker buildMarker(latitude, longitude, {isRoot = false}) {
    return Marker(
      width: 80.0,
      height: 80.0,
      point: LatLng(latitude, longitude),
      builder: (_) => buildPointer(isRoot: isRoot),
    );
  }

  Widget buildPointer({isRoot = false}) {
    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: isRoot
            ? Colors.blue.withOpacity(0.2)
            : Colors.red.withOpacity(0.2),
      ),
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: isRoot ? Colors.blue : Colors.red,
        ),
        child: Center(
          child: isRoot
              ? Text('')
              : Text('E', style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: parkingBloc.parkings,
      builder: (context, AsyncSnapshot<ParkingList> snapshot) {
        if (snapshot.hasData) {
          List<ParkingModel> items = snapshot.data.items;

          markers = [];

          if (widget.latitude != null && widget.longitude != null) {
            markers.add(
                buildMarker(widget.latitude, widget.longitude, isRoot: true));
          }

          items.forEach((item) {
            markers.add(buildMarker(item.latitude, item.longitude));
          });
        }

        return FlutterMap(
          options: MapOptions(
            center: widget.latitude != null && widget.longitude != null
                ? LatLng(widget.latitude, widget.longitude)
                : null,
            zoom: 18.0,
          ),
          layers: [
            new TileLayerOptions(
              urlTemplate: "https://api.tiles.mapbox.com/v4/"
                  "{id}/{z}/{x}/{y}@2x.png?access_token=" +
                  MAP_API,
              additionalOptions: {
                'accessToken': MAP_API,
                'id': 'mapbox.streets',
              },
            ),
            MarkerLayerOptions(markers: markers),
          ],
        );
      },
    );
  }
}
