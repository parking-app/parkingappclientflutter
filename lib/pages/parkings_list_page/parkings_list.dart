import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:location/location.dart' as GPSLocation;
import 'package:parkingclientapp/bloc/parking_bloc.dart';
import 'package:parkingclientapp/models/parking_models.dart';
import 'package:parkingclientapp/models/response_models.dart';
import 'package:parkingclientapp/pages/parkings_list_page/card_list_parking.dart';
import 'package:parkingclientapp/pages/parkings_list_page/map_list_parking.dart';
import 'package:parkingclientapp/pages/parkings_list_page/parkings_list_filter.dart';
import 'package:parkingclientapp/pages/user_page/user_page.dart';

class ParkingListPage extends StatefulWidget {
  final location = new GPSLocation.Location();

  @override
  ParkingListPageState createState() {
    return new ParkingListPageState();
  }
}

class ParkingListPageState extends State<ParkingListPage> {
  ResponseMeta meta = new ResponseMeta();

  List<Marker> markers = [];

  String search;

  int radius = 3;

  double latitude;

  double longitude;

  bool loading = false;

  Future<void> updateLocation() async {
    try {
      Map<String, double> currentLocation = await widget.location.getLocation();
      latitude = currentLocation['latitude'];
      longitude = currentLocation['longitude'];
      setState(() => {});
    } catch (e) {
      latitude = null;
      longitude = null;
      _showDialogErrorLatLong();
    }
  }

  void _showDialogErrorLatLong() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
            title: Text('Ops!'),
            content: Text('Encontramos dificuldade para encontrar sua posição'),
            actions: <Widget>[
              FlatButton(
                child: Text('Entendi'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ),
    );
  }

  Future<Map<String, dynamic>> _makeMeta({limit = 10}) async {
    await updateLocation();

    Map<String, dynamic> query = {
      'page': meta.page,
      'limit': limit,
      'search': search,
      'radius': radius,
      'latitude': latitude,
      'longitude': longitude
    };

    return query;
  }

  Future<void> sendSearch({limit = 10}) async {

    ParkingList parking = await parkingBloc.fetchParkings(query: await _makeMeta(limit: limit));

    meta = parking.meta;

  }

  void _updateRadius(radius) {
    this.radius = radius;

    this.sendSearch();
  }

  void _goToFilters() {
    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (_) => ParkingListFilterPage(
              _updateRadius,
              initialRadius: radius,
            ),
      ),
    );
  }

  void _goToUserPage(){
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => UserPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            leading:   IconButton(
              onPressed: () => _goToUserPage(),
              icon: Icon(Icons.person),
              color: Colors.white,
            ),
            title: Text('Parkings'),
            centerTitle: true,
            actions: <Widget>[
              IconButton(
                onPressed: () => _goToFilters(),
                icon: Icon(Icons.filter_list),
                color: Colors.white,
              )
            ],
            bottom: TabBar(
              tabs: <Widget>[
                Tab(icon: Icon(Icons.list)),
                Tab(icon: Icon(Icons.map)),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              CardListParking(sendSearch, latitude, longitude),
              MapListParking(sendSearch, latitude, longitude)
            ],
          )),
    );
  }
}


