import 'package:flutter/material.dart';
import 'package:parkingclientapp/bloc/parking_bloc.dart';
import 'package:parkingclientapp/models/parking_models.dart';
import 'package:url_launcher/url_launcher.dart';

class CardListParking extends StatefulWidget {
  final double latitude;
  final double longitude;
  final Function sendSearch;

  CardListParking(this.sendSearch, this.latitude, this.longitude);

  @override
  CardListParkingState createState() {
    return new CardListParkingState();
  }
}

class CardListParkingState extends State<CardListParking> {
  bool loading = false;

  @override
  initState() {
    super.initState();
    sendSearch();
  }

  Future<void> sendSearch({bool changeLoading = true}) async {
    setState(() {
      loading = changeLoading;
    });

    await widget.sendSearch();

    setState(() {
      loading = false;
    });
  }

  Widget buildGridItems(List<ParkingModel> items) {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return buildCardItem(items[index]);
      },
    );
  }

  Widget buildCardItem(ParkingModel item) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: NetworkImage(
                  'http://www.juventus.com.br/wp-content/uploads/2011/08/estacionamento.jpg',
                ),
              ),
            ),
          ),
          ListTile(
            onTap: () => goToParking(item),
            title: Text(item.name),
            subtitle: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(item.fullAddress),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('Distancia: ' + item.distance.toString() + ' km'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('Vagas: ' + item.totalVacancies.toString()),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 5.0),
                      child: Text('Nota: ' + (item.averageRatings > 0 ? item.averageRatings.toString() : 'NOVA')),
                    )
                  ],
                ),
                Container(
                  transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                  child: Transform.rotate(
                    angle: 5.9,
                    child: IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () => openMaps(item),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildEmptyList() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          children: <Widget>[
            Image.asset('assets/404.png'),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                'Ops, não encontramos nenhum estacionamento, tente trocar os filtros',
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showErrorSnackBar(String string) {
    var snackBar = SnackBar(
      content: Text(string),
      duration: Duration(seconds: 3),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => sendSearch(changeLoading: false),
      child: StreamBuilder(
        stream: parkingBloc.parkings,
        builder: (context, AsyncSnapshot<ParkingList> snapshot) {
          if (!snapshot.hasData || loading) {
            return Center(child: CircularProgressIndicator());
          }

          if (snapshot.data.items.isEmpty) {
            return buildEmptyList();
          }

          if (snapshot.hasError) {
            showErrorSnackBar(snapshot.error.toString());
            return buildEmptyList();
          }

          List<ParkingModel> items = snapshot.data.items;

          return buildGridItems(items);
        },
      ),
    );
  }

  openMaps(ParkingModel item) async {
    String url =
        'https://www.google.com/maps/dir/?api=1&origin=${widget.latitude},${widget.longitude}&destination=${item.latitude},${item.longitude}&travelmode=driving&dir_action=navigate';

    print(url);
    if (!await canLaunch(url)) {
      return showErrorSnackBar('Não foi possivel abrir o mapa');

    }

    return await launch(url);

  }

  goToParking(ParkingModel item) {}
}
