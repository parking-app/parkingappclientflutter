import 'package:flutter/material.dart';
import 'package:parkingclientapp/louisk/base_provider.dart';
import 'package:parkingclientapp/louisk/loading_button.dart';
import 'package:parkingclientapp/louisk/validators.dart';
import 'package:parkingclientapp/providers/auth_provider.dart';

class RegisterPage extends StatefulWidget {
  final AuthProvider authProvider = new AuthProvider();

  @override
  _RegisterPageState createState() {
    return new _RegisterPageState();
  }
}

class _RegisterPageState extends State<RegisterPage> {
  bool _loading = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final Map<String, Map<String, dynamic>> controllers = {
    'name': {
      'errorText': null,
      'controller': new TextEditingController(),
    },
    'email': {
      'errorText': null,
      'controller': new TextEditingController(),
    },
    'password': {
      'errorText': null,
      'controller': new TextEditingController(),
    },
    'password_confirmation': {
      'errorText': null,
      'controller': new TextEditingController(),
    },
  };

  _validate() async {
    _clearErrorText();

    if (_formKey.currentState.validate()) {
      _sendForm();
    }
  }

  _sendForm() async {
    try {
      setState(() {
        _loading = true;
      });

      await widget.authProvider.register(
        controllers['name']['controller'].text,
        controllers['email']['controller'].text,
        controllers['password']['controller'].text,
        controllers['password_confirmation']['controller'].text,
      );

      Navigator.of(context).pushReplacementNamed('/main');
    } on ValidatorException catch (validator) {
      controllers.forEach((key, input) {
        controllers[key]['errorText'] = validator.getMessage(key);
      });
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }

  _clearErrorText() {
    setState(() {
      controllers.forEach((key, input) {
        controllers[key]['errorText'] = null;
      });
    });
  }

  _pushToLogin(context) {
    Navigator.of(context).pushReplacementNamed('/login');
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            controller: controllers['name']['controller'],
            decoration: InputDecoration(
              labelText: 'Nome',
              errorText: controllers['name']['errorText'],
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
                ValidatorRules.min(3)
              ];

              Validator validator = Validator(rules, 'name');

              return validator.validate(value);
            },
          ),
          TextFormField(
            controller: controllers['email']['controller'],
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              labelText: 'Email',
              errorText: controllers['email']['errorText'],
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
                ValidatorRules.Email
              ];

              Validator validator = Validator(rules, 'email');

              return validator.validate(value);
            },
          ),
          TextFormField(
            controller: controllers['password']['controller'],
            decoration: InputDecoration(
              labelText: 'Senha',
              errorText: controllers['password']['errorText'],
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
                ValidatorRules.min(6)
              ];

              Validator validator = Validator(rules, 'password');

              return validator.validate(value);
            },
          ),
          TextFormField(
            controller: controllers['password_confirmation']['controller'],
            decoration: InputDecoration(
              labelText: 'Confirmação de senha',
              errorText: controllers['password_confirmation']['errorText'],
              errorMaxLines: 2,
            ),
            validator: (value) {
              List<String> rules = [
                ValidatorRules.Required,
                ValidatorRules.min(6),
                ValidatorRules.confirmation(
                  'password',
                  controllers['password']['controller'].text,
                )
              ];

              Validator validator = Validator(rules, 'password_confirmation');

              return validator.validate(value);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 25, vertical: 100),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: _form(),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: LoadingButton(
                  RaisedButton(
                    onPressed: () => _validate(),
                    child: Text('Cadastrar'),
                  ),
                  _loading,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () => _pushToLogin(context),
                  child: Text('Login'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
