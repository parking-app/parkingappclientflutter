class Validator {
  List<String> _rules = [];
  String _attribute;

  Validator(List<String> rules, String attribute) {
    _rules = rules;
    _attribute = attribute;
  }

  String validate(value) {
    for (int i = 0; i < _rules.length; i++) {
      String rule = _rules[i];
      String optional = '';
      String firstOptional = '';
      String secondOptional = '';

      if (rule.contains(':')) {
        List<String> ruleAndOptional = rule.split(':');

        rule = ruleAndOptional[0];
        optional = ruleAndOptional[1];
      }

      if(optional.contains(',')) {
        List<String> optionals = optional.split(',');

        firstOptional = optionals[0];
        secondOptional = optionals[1];
      }

      switch (rule) {
        case ValidatorRules.Email:
          if (!Validator.validateEmail(value)) {
            return ValidatorMessages.buildMessage(
                ValidatorMessages.InvalidEmail,
                this._attribute
            );
          }
          break;
        case ValidatorRules.Required:
          if (!Validator.validateRequire(value)) {
            return ValidatorMessages.buildMessage(
                ValidatorMessages.Required,
                this._attribute
            );
          }
          break;
        case ValidatorRules.Min:
          if (!Validator.validateMin(value, int.parse(optional))) {
            return ValidatorMessages.buildMessage(
                ValidatorMessages.Min,
                this._attribute,
                optional: optional
            );
          }
          break;
        case ValidatorRules.Max:
          if (!Validator.validateMax(value, int.parse(optional))) {
            return ValidatorMessages.buildMessage(
                ValidatorMessages.Max,
                this._attribute,
                optional: optional
            );
          }
          break;
        case ValidatorRules.Confirmation:
          if (!Validator.validateConfirmation(value, secondOptional)) {
            return ValidatorMessages.buildMessage(
                ValidatorMessages.Confirmation,
                this._attribute,
                optional: firstOptional
            );
          }
          break;
      }
    }

    return null;
  }

  static validateRequire(String value) {
    return value.length > 0;
  }

  static validateEmail(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);

    return regex.hasMatch(email);
  }

  static validateMin(String value, int min) {
    return value.length >= min;
  }

  static validateMax(String value, int max) {
    return value.length <= max;
  }

  static validateConfirmation(String value, String confirmation) {
    return value == confirmation;
  }

  static validatePassword(String password) {}
}

class ValidatorRules {
  static const Email = 'email';
  static const Required = 'required';
  static const Min = 'min';
  static const Max = 'max';
  static const Confirmation = 'confirmation';

  static min(int value){
    return ValidatorRules.Min + ':' + value.toString();
  }

  static max(int value){
    return ValidatorRules.Max + ':' + value.toString();
  }

  static confirmation(String attribute, String value) {
    return ValidatorRules.Confirmation + ':'+ attribute + ',' + value;
  }
}

class ValidatorMessages {
  static const String _replaceAttribute = ':attribute';
  static const String _replaceOptional = ':optional';

  static String buildMessage(
      String message, String attribute, {String optional = ''}) {
    return message.replaceAll(_replaceAttribute, attribute).replaceAll(_replaceOptional, optional);
  }

  // ignore: unnecessary_brace_in_string_interps
  static const InvalidEmail = 'O campo ${_replaceAttribute} é inválido';

  // ignore: unnecessary_brace_in_string_interps
  static const Required = 'O campo ${_replaceAttribute} é obrigatorio';

  // ignore: unnecessary_brace_in_string_interps
  static const Min = 'O campo ${_replaceAttribute} precisa ser maior que ${_replaceOptional} caracteres';

  // ignore: unnecessary_brace_in_string_interps
  static const Max = 'O campo ${_replaceAttribute} precisa ser menor que ${_replaceOptional} caracteres';

  // ignore: unnecessary_brace_in_string_interps
  static const Confirmation = 'O campo ${_replaceAttribute} precisa ser igual ao campo ${_replaceOptional}';
}
