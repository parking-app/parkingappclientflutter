import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' show Client, Response;
import 'package:parkingclientapp/config.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseProvider {
  static const OK = 200;
  static const BAD_REQUEST = 400;
  static const UNAUTHORIZED = 401;

  static const INTERNAL = 500;

  String _base = API_URL;
  Client client = Client();

  Future<Map<String, dynamic>> get(String url,
      {Map<String, dynamic> query = const {}}) async {
    final String realUrl = _buildUrl(url, query: query);

    final Map<String, String> headers = await _makeHeaders();

    final response = await client.get(realUrl, headers: headers);

    return _parseResponse(response);
  }

  Future<Map<String, dynamic>> post(
      String url, Map<String, dynamic> body) async {
    final String realUrl = _buildUrl(url);
    final Map<String, String> headers = await _makeHeaders();

    final response = await client.post(realUrl, headers: headers, body: body);

    return _parseResponse(response);
  }

  Future<Map<String, dynamic>> put(
      String url, Map<String, dynamic> body) async {
    final String realUrl = _buildUrl(url);
    final Map<String, String> headers = await _makeHeaders();

    final response = await client.put(realUrl, headers: headers, body: body);

    return _parseResponse(response);
  }

  Future<Object> _makeHeaders() async {
    String token = await getToken();

    Map<String, String> headers = {
      HttpHeaders.acceptHeader: 'application/json',
    };

    if (token != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer $token';
    }

    return headers;
  }

  String _buildUrl(String url, {Map<String, dynamic> query = const {}}) {
    String realUrl = _base + '/' + url;

    if (query.length > 0) {
      realUrl += '?';

      query.forEach((key, value) {
        realUrl = realUrl + '&' + key + '=' + value.toString();
      });
    }

    print(realUrl);

    return realUrl;
  }

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(JWT_TOKEN);
  }

  void _setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString(JWT_TOKEN, token);
  }

  Map<String, dynamic> _parseResponse(Response response) {
    print(response.statusCode);
    if (response.headers.containsKey('new_token')) {
      this._setToken(response.headers['new_token']);
    }

    final Map<String, dynamic> responseParsed = json.decode(response.body);

    if (response.statusCode == BaseProvider.BAD_REQUEST &&
        responseParsed.containsKey('errors')) {
      throw new ValidatorException(
          json.decode(json.encode(responseParsed['errors'])));
    }

    if (response.statusCode == BaseProvider.UNAUTHORIZED) {
      throw new UnauthorizedException();
    }

    if (response.statusCode == BaseProvider.INTERNAL) {
      throw new UnauthorizedException();
    }

    if (responseParsed.containsKey('token')) {
      this._setToken(responseParsed['token']);
    }

    return responseParsed;
  }
}

class ValidatorException implements Exception {
  Map<String, dynamic> _messages = {};

  ValidatorException(this._messages);

  String getMessage(field) {
    if (!this._messages.containsKey(field)) {
      return null;
    }

    String message = this._messages[field].first;

    if (message == null) {
      return null;
    }

    return message[0].toUpperCase() + message.substring(1, message.length - 1);
  }
}

class UnauthorizedException implements Exception {}
