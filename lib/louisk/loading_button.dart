import 'package:flutter/material.dart';

class LoadingButton extends StatefulWidget {
  final Widget button;
  final bool isLoading;

  LoadingButton(this.button, this.isLoading);


  @override
  _LoadingButtonState createState() => _LoadingButtonState();
}

class _LoadingButtonState extends State<LoadingButton> {
  @override
  Widget build(BuildContext context) {
    if(widget.isLoading){
      return CircularProgressIndicator();
    }

    return widget.button;
  }
}
