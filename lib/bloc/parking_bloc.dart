import 'package:parkingclientapp/models/parking_models.dart';
import 'package:parkingclientapp/providers/parking_provider.dart';
import 'package:rxdart/rxdart.dart';

class ParkingBloc {
  final _provider = ParkingProvider();
  final _parkingFetcher = PublishSubject<ParkingList>();

  Observable<ParkingList> get parkings => _parkingFetcher.stream;

  fetchParkings({Map<String, dynamic> query = const {}}) async {
    ParkingList parkingList = await _provider.fetchParkingList(query: query);

    _parkingFetcher.sink.add(parkingList);

    return parkingList;
  }

  dispose() {
    _parkingFetcher.close();
  }
}

final parkingBloc = ParkingBloc();
