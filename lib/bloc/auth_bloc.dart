import 'package:parkingclientapp/models/client_models.dart';
import 'package:parkingclientapp/providers/auth_provider.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc {
  final _provider = AuthProvider();
  final _authFetcher = PublishSubject<ClientModel>();

  Observable<ClientModel> get profile => _authFetcher.stream;

  fetchProfile() async {
    ClientModel profile = await _provider.getProfile();

    _authFetcher.sink.add(profile);
  }

  dispose() {
    _authFetcher.close();
  }
}

final authBloc = AuthBloc();
